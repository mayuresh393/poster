package com.jagmit.android.postgram.Pojo;

/**
 * Created by MAYURESH NIMBALKAR on 23/3/19.
 */
public class Friend {

    private String profileimage;
    private String fullname;
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
}
