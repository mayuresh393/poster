package com.jagmit.android.postgram.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.Constants;
import com.jagmit.android.postgram.Utils.FireBaseConstants;

import java.util.HashMap;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class SetupActivity extends AppCompatActivity {
    private static final int RESULT_LOAD_IMAGE = 1;
    private Button buttonSave;
    private EditText editTextUserName, editTextFullName, editTextCountryName;
    private CircleImageView circleImageViewProfile;
    private RadioButton radioButtonMale, radioButtonFemale;
    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseAuth;
    private String gender;
    private Uri filePath;
    private String profilePath;
    private String currentUserId;
    private StorageReference storageReference;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        firebaseAuth = FirebaseAuth.getInstance();
        currentUserId = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_USER).child(currentUserId);
        storageReference = FirebaseStorage.getInstance().getReference().child("Profile Images");

        initView();

        initViewListeners();
    }

    private void initViewListeners() {
        circleImageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent imagePickerIntent = new Intent();
                imagePickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                imagePickerIntent.setType("image/*");
                startActivityForResult(Intent.createChooser(imagePickerIntent, "Select Profile Pic"), RESULT_LOAD_IMAGE);
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    progressDialog = new ProgressDialog(SetupActivity.this);
                    progressDialog.setTitle("Please wait while saving details");
                    progressDialog.setCancelable(false);
                    progressDialog.setIndeterminate(true);
                    progressDialog.show();
                    saveUserDetailsToDatabase();
                }
            }
        });

        radioButtonMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkRadioButton(buttonView, isChecked);
            }
        });

        radioButtonFemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkRadioButton(buttonView, isChecked);
            }
        });
    }

    private void saveUserDetailsToDatabase() {
        String username = editTextUserName.getText().toString().trim();
        String fullname = editTextFullName.getText().toString().trim();
        String countryName = editTextCountryName.getText().toString().trim();

        HashMap<String, Object> userMap = new HashMap<>();

        userMap.put(FireBaseConstants.USER_NAME, username);
        userMap.put(FireBaseConstants.FULL_NAME, fullname);
        userMap.put(FireBaseConstants.COUNTRY, countryName);
        userMap.put(FireBaseConstants.STATUS, Constants.DEFAULT_STATUS);
        userMap.put(FireBaseConstants.GENDER, gender);
        userMap.put(FireBaseConstants.DOB, Constants.DEFAULT_NONE_VALUE);
        userMap.put(FireBaseConstants.RELATION_STATUS, Constants.DEFAULT_NONE_VALUE);

        databaseReference.updateChildren(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(SetupActivity.this, "User details are saved in database", Toast.LENGTH_SHORT).show();
                    sendUserToHomeActivity();
                    progressDialog.dismiss();
                } else {
                    String message = Objects.requireNonNull(task.getException()).getMessage().trim();
                    progressDialog.dismiss();
                    Toast.makeText(SetupActivity.this, "Error occurred while saving user details " + message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void sendUserToHomeActivity() {
        Intent homeIntent = new Intent(SetupActivity.this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(homeIntent);
        finish();
    }

    private void checkRadioButton(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.radioButtonMale) {
            if (isChecked) {
                gender = radioButtonMale.getText().toString().trim();
                radioButtonFemale.setChecked(false);
            }
        }
        if (buttonView.getId() == R.id.radioButtonFemale) {
            if (isChecked) {
                gender = radioButtonFemale.getText().toString().trim();
                radioButtonMale.setChecked(false);
            }
        }
    }

    private boolean checkValidation() {
        String username = editTextUserName.getText().toString().trim();
        String fullname = editTextFullName.getText().toString().trim();
        String countryName = editTextCountryName.getText().toString().trim();

        if (filePath == null) {
            Toast.makeText(SetupActivity.this, "Please Select Profile Pic", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (username.isEmpty()) {
            editTextUserName.setError(Constants.USERNAME_ERROR);
            return false;
        }
        if (fullname.isEmpty()) {
            editTextFullName.setError(Constants.FULLNAME_ERROR);
            return false;
        }
        if (countryName.isEmpty()) {
            editTextCountryName.setError(Constants.COUNTRY_ERROR);
            return false;
        }
        if (gender.isEmpty()) {
            radioButtonFemale.setError(Constants.GENDER_ERROR);
            return false;
        }
        return true;
    }

    private void initView() {
        editTextUserName = findViewById(R.id.edtTextUsername);
        editTextFullName = findViewById(R.id.edtTextFullName);
        editTextCountryName = findViewById(R.id.edtTextCountryName);
        circleImageViewProfile = findViewById(R.id.circleImageViewProfile);
        buttonSave = findViewById(R.id.btnSave);
        radioButtonMale = findViewById(R.id.radioButtonMale);
        radioButtonFemale = findViewById(R.id.radioButtonFemale);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
            filePath = data.getData();
            circleImageViewProfile.setImageURI(filePath);
            circleImageViewProfile.setScaleType(ImageView.ScaleType.CENTER_CROP);
            uploadImageToFireBase(filePath);
        }
    }

    private void uploadImageToFireBase(Uri filePath) {
        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Please wait while uploading profile picture");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();

            final StorageReference storageReferenceFilePath = storageReference.child(currentUserId+".jpg");

            storageReferenceFilePath.putFile(filePath).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        progressDialog.dismiss();
                        Toast.makeText(SetupActivity.this,"Error Occurred while Uploading "+ Objects.requireNonNull(task.getException()).getMessage(),Toast.LENGTH_SHORT).show();
                    }
                    return storageReferenceFilePath.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri taskResult = task.getResult();
                        if (taskResult != null) {
                            profilePath = taskResult.toString();
                        }
                    }
                    else {
                        progressDialog.dismiss();
                        Toast.makeText(SetupActivity.this,"Error Occurred while Uploading "+ Objects.requireNonNull(task.getException()).getMessage(),Toast.LENGTH_SHORT).show();
                    }
                    databaseReference.child(FireBaseConstants.PROFILE_IMAGE).setValue(profilePath).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(SetupActivity.this, "Profile Uploaded Successfully", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }
                            else{
                                String message = Objects.requireNonNull(task.getException()).getMessage();
                                Toast.makeText(SetupActivity.this, "Error Occurred while Uploading Profile Pic " + message, Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }
                        }
                    });
                }
            });
        }
    }
}
