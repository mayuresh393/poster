package com.jagmit.android.postgram.Activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.Constants;
import com.jagmit.android.postgram.Utils.FireBaseConstants;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendProfileActivity extends AppCompatActivity {
    private TextView usernameTextView, userProfNameTextView, userStatusTextView, userCountryTextView, userGenderTextView, userRelationshipStatusTextView, userDobTextView;
    private CircleImageView userProfileCircleImageView;
    private Button sendAcceptRequestButton,declineRequestButton;

    private DatabaseReference friendRequestRef,usersRef,friendsRef;
    private FirebaseAuth firebaseAuth;
    private String senderUserId,receiverUserId,currentState,currentDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_profile);

        firebaseAuth = FirebaseAuth.getInstance();
        senderUserId = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        receiverUserId = Objects.requireNonNull(Objects.requireNonNull(getIntent().getExtras()).get(Constants.VISIT_USER_ID)).toString();
        usersRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_USER);
        friendRequestRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_FRIEND_REQUEST);
        friendsRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_FRIENDS);

        initView();

        initListener();

        declineRequestButton.setVisibility(View.INVISIBLE);
        declineRequestButton.setEnabled(false);

        if (!senderUserId.equals(receiverUserId)){
            sendAcceptRequestButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendAcceptRequestButton.setEnabled(false);
                    if (currentState.equals(Constants.DEFAULT_FRIEND_STATUS)){
                        sendFriendRequest();
                    }
                    if (currentState.equals(Constants.REQUEST_SENT)){
                        cancelFriendRequest();
                    }
                    if (currentState.equals(Constants.REQUEST_RECEIVED)){
                        acceptFriendRequest();
                    }
                    if (currentState.equals(Constants.FRIENDS)){
                        unfriendUser();
                    }
                }
            });
        }
        else {
            declineRequestButton.setVisibility(View.INVISIBLE);
            sendAcceptRequestButton.setVisibility(View.INVISIBLE);
        }
    }

    private void unfriendUser() {

        friendsRef.child(senderUserId).child(receiverUserId)
                .removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            friendsRef.child(receiverUserId).child(senderUserId)
                                    .removeValue()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){
                                                sendAcceptRequestButton.setEnabled(true);
                                                currentState = Constants.DEFAULT_FRIEND_STATUS;
                                                sendAcceptRequestButton.setText(Constants.SEND_REQUEST);

                                                declineRequestButton.setVisibility(View.INVISIBLE);
                                                declineRequestButton.setEnabled(false);
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void acceptFriendRequest() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
        currentDate = simpleDateFormat.format(calendar.getTime());

        friendsRef.child(senderUserId).child(receiverUserId).child("date")
                .setValue(currentDate)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){

                            friendsRef.child(receiverUserId).child(senderUserId).child(FireBaseConstants.DATE)
                                    .setValue(currentDate)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){

                                                friendRequestRef.child(senderUserId).child(receiverUserId)
                                                        .removeValue()
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()){
                                                                    friendRequestRef.child(receiverUserId).child(senderUserId)
                                                                            .removeValue()
                                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                @Override
                                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                                    if (task.isSuccessful()){
                                                                                        sendAcceptRequestButton.setEnabled(true);
                                                                                        currentState = Constants.FRIENDS;
                                                                                        sendAcceptRequestButton.setText(Constants.UNFRIEND);

                                                                                        declineRequestButton.setVisibility(View.INVISIBLE);
                                                                                        declineRequestButton.setEnabled(false);
                                                                                    }
                                                                                }
                                                                            });
                                                                }
                                                            }
                                                        });

                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void cancelFriendRequest() {

        friendRequestRef.child(senderUserId).child(receiverUserId)
                .removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            friendRequestRef.child(receiverUserId).child(senderUserId)
                                    .removeValue()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){
                                                sendAcceptRequestButton.setEnabled(true);
                                                currentState = Constants.DEFAULT_FRIEND_STATUS;
                                                sendAcceptRequestButton.setText(Constants.SEND_REQUEST);

                                                declineRequestButton.setVisibility(View.INVISIBLE);
                                                declineRequestButton.setEnabled(false);
                                            }
                                        }
                                    });
                        }
                    }
                });

    }

    private void sendFriendRequest() {
        friendRequestRef.child(senderUserId).child(receiverUserId)
                .child(Constants.REQUEST_TYPE).setValue(Constants.SENT)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            friendRequestRef.child(receiverUserId).child(senderUserId)
                                    .child(Constants.REQUEST_TYPE).setValue(Constants.RECEIVED)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){
                                                sendAcceptRequestButton.setEnabled(true);
                                                currentState = Constants.REQUEST_SENT;
                                                sendAcceptRequestButton.setText(Constants.CANCEL_REQUEST);

                                                declineRequestButton.setVisibility(View.INVISIBLE);
                                                declineRequestButton.setEnabled(false);
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void initListener() {

        usersRef.child(receiverUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String profileImage = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.PROFILE_IMAGE).getValue()).toString();
                    String username = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.USER_NAME).getValue()).toString();
                    String fullname = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.FULL_NAME).getValue()).toString();
                    String gender = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.GENDER).getValue()).toString();
                    String status = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.STATUS).getValue()).toString();
                    String country = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.COUNTRY).getValue()).toString();
                    String dob = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.DOB).getValue()).toString();
                    String relationshipStatus = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.RELATION_STATUS).getValue()).toString();


                    Picasso.get().load(profileImage).placeholder(R.drawable.ic_logo_profile).into(userProfileCircleImageView);

                    usernameTextView.setText("@" + username);
                    userProfNameTextView.setText(fullname);
                    userGenderTextView.setText("Gender: " + gender);
                    userStatusTextView.setText(status);
                    userCountryTextView.setText("Country: " + country);
                    userDobTextView.setText("DOB: " + dob);
                    userRelationshipStatusTextView.setText("Relationship Status: " + relationshipStatus);

                    maintaneButtonState();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void maintaneButtonState() {

        friendRequestRef.child(senderUserId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(receiverUserId)){
                    String requestType = Objects.requireNonNull(dataSnapshot.child(receiverUserId).child(Constants.REQUEST_TYPE).getValue()).toString();

                    if (requestType.equals(Constants.SENT)){
                        currentState = Constants.REQUEST_SENT;
                        sendAcceptRequestButton.setText(Constants.CANCEL_REQUEST);

                        declineRequestButton.setVisibility(View.INVISIBLE);
                        declineRequestButton.setEnabled(false);
                    }
                    else if (requestType.equals(Constants.RECEIVED)){
                        currentState = Constants.REQUEST_RECEIVED;
                        sendAcceptRequestButton.setText(Constants.ACCEPT_REQUEST);

                        declineRequestButton.setVisibility(View.VISIBLE);
                        declineRequestButton.setEnabled(true);

                        declineRequestButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                cancelFriendRequest();
                            }
                        });
                    }
                }
                else {
                    friendsRef.child(senderUserId)
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.hasChild(receiverUserId)){
                                        currentState = Constants.FRIENDS;
                                        sendAcceptRequestButton.setText(Constants.UNFRIEND);

                                        declineRequestButton.setVisibility(View.INVISIBLE);
                                        declineRequestButton.setEnabled(false);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initView() {
        usernameTextView = findViewById(R.id.friend_profile_username);
        userProfNameTextView = findViewById(R.id.friend_profile_name);
        userCountryTextView = findViewById(R.id.friend_profile_country);
        userDobTextView = findViewById(R.id.friend_profile_dob);
        userStatusTextView = findViewById(R.id.friend_profile_status);
        userRelationshipStatusTextView = findViewById(R.id.friend_profile_relation_status);
        userGenderTextView = findViewById(R.id.friend_profile_gender);

        userProfileCircleImageView = findViewById(R.id.friend_profile_pic);

        sendAcceptRequestButton = findViewById(R.id.friend_send_request);
        declineRequestButton = findViewById(R.id.friend_decline_request);

        currentState = Constants.DEFAULT_FRIEND_STATUS;

    }
}
