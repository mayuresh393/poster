package com.jagmit.android.postgram.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jagmit.android.postgram.Utils.Constants;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.FireBaseConstants;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private EditText usernameEditText,userProfNameEditText,userStatusEditText,userCountryEditText,userGenderEditText,userRelationshipStatusEditText,userDobEditText;
    private Button updateSettingsButton;
    private CircleImageView circleImageViewProfileSettings;
    private DatabaseReference settingsUserRef;
    private FirebaseAuth firebaseAuth;
    private String currentUserId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        firebaseAuth = FirebaseAuth.getInstance();
        currentUserId = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        settingsUserRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_USER).child(currentUserId);

        initView();
    }

    private void initView() {
        toolbar = findViewById(R.id.settings_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setTitle(Constants.ACCOUNT_SETTINGS);


        usernameEditText = findViewById(R.id.settings_username);
        userProfNameEditText = findViewById(R.id.settings_profile_name);
        userCountryEditText = findViewById(R.id.settings_profile_country);
        userDobEditText = findViewById(R.id.settings_profile_dob);
        userStatusEditText = findViewById(R.id.settings_status);
        userRelationshipStatusEditText = findViewById(R.id.settings_profile_relationship_status);
        userGenderEditText = findViewById(R.id.settings_profile_gender);

        circleImageViewProfileSettings = findViewById(R.id.settingProfileImage);
        updateSettingsButton = findViewById(R.id.settings_profile_update_Btn);


        settingsUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    String profileImage = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.PROFILE_IMAGE).getValue()).toString();
                    String username = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.USER_NAME).getValue()).toString();
                    String fullname = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.FULL_NAME).getValue()).toString();
                    String gender = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.GENDER).getValue()).toString();
                    String status = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.STATUS).getValue()).toString();
                    String country = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.COUNTRY).getValue()).toString();
                    String dob = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.DOB).getValue()).toString();
                    String relationshipStatus = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.RELATION_STATUS).getValue()).toString();


                    Picasso.get().load(profileImage).placeholder(R.drawable.ic_logo_profile).into(circleImageViewProfileSettings);

                    if (!username.equals(Constants.DEFAULT_NONE_VALUE)){
                        usernameEditText.setText(username);
                    }
                    if (!fullname.equals(Constants.DEFAULT_NONE_VALUE)){
                        userProfNameEditText.setText(fullname);
                    }
                    if (!gender.equals(Constants.DEFAULT_NONE_VALUE)){
                        userGenderEditText.setText(gender);
                    }
                    if (!status.equals(Constants.DEFAULT_NONE_VALUE)){
                        userStatusEditText.setText(status);
                    }
                    if (!country.equals(Constants.DEFAULT_NONE_VALUE)){
                        userCountryEditText.setText(country);
                    }
                    if (!dob.equals(Constants.DEFAULT_NONE_VALUE)){
                        userDobEditText.setText(dob);
                    }
                    if (!relationshipStatus.equals(Constants.DEFAULT_NONE_VALUE)){
                        userRelationshipStatusEditText.setText(relationshipStatus);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
