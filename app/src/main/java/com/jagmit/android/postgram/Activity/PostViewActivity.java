package com.jagmit.android.postgram.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jagmit.android.postgram.Utils.Constants;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.FireBaseConstants;
import com.squareup.picasso.Picasso;

import java.util.Objects;

public class PostViewActivity extends AppCompatActivity {

    private ImageView postImageView;
    private TextView descriptionTextView;
    private Button editButton,deleteButton;
    private DatabaseReference clickPostRef;
    private FirebaseAuth firebaseAuth;
    private String currentUserId,databaseUserId,description,image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firebaseAuth = FirebaseAuth.getInstance();

        currentUserId = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();



        setContentView(R.layout.activity_post_view);
        String postKey = Objects.requireNonNull(Objects.requireNonNull(getIntent().getExtras()).get(Constants.POST_KEY)).toString();
        clickPostRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_POSTS).child(postKey);

        initView();


        initListeners();

    }

    private void initListeners() {
        clickPostRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    description = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.DESCRIPTION).getValue()).toString();
                    image = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.POST_IMAGE).getValue()).toString();
                    databaseUserId = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.UID).getValue()).toString();

                    descriptionTextView.setText(description);
                    Picasso.get().load(image).into(postImageView);

                    if (currentUserId.equals(databaseUserId)){
                        deleteButton.setVisibility(View.VISIBLE);
                        editButton.setVisibility(View.VISIBLE);
                    }

                    editButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editCurrentUserPost(description);
                        }
                    });
                    
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCurrentPost();
            }
        });


    }

    private void editCurrentUserPost(String description) {
        AlertDialog.Builder builder= new AlertDialog.Builder(PostViewActivity.this);
        builder.setTitle(Constants.BUILDER_TITLE);

        final EditText inputField = new EditText(PostViewActivity.this);
        inputField.setText(description);
        builder.setView(inputField);

        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clickPostRef.child("description").setValue(inputField.getText().toString().trim());
                Toast.makeText(PostViewActivity.this,"Post updated Successfully",Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        Dialog dialog = builder.create();
        dialog.show();
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(R.color.colorBackground);
    }

    private void deleteCurrentPost() {
        clickPostRef.removeValue();
        sendUserToHomeActivity();
        Toast.makeText(PostViewActivity.this,Constants.DELETE_POST_MESSAGE,Toast.LENGTH_SHORT).show();
    }

    private void sendUserToHomeActivity() {
        Intent homeIntent = new Intent(PostViewActivity.this,HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(homeIntent);
        finish();
    }

    private void initView() {
        postImageView = findViewById(R.id.imageViewPost);
        descriptionTextView = findViewById(R.id.txtViewDescription);
        deleteButton = findViewById(R.id.btnDelete);
        editButton = findViewById(R.id.btnEdit);

        deleteButton.setVisibility(View.INVISIBLE);
        editButton.setVisibility(View.INVISIBLE);
    }
}
