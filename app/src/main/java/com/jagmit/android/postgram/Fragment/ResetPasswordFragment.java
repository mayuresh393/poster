package com.jagmit.android.postgram.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.Constants;

import java.util.Objects;

public class ResetPasswordFragment extends Fragment {
    private static ResetPasswordFragment resetPasswordFragment = null;
    private EditText resetPasswordEmailEditText;
    private Button sendButton;
    private Toolbar toolbar;
    private FirebaseAuth firebaseAuth;


    public static ResetPasswordFragment getInstance() {
        if (resetPasswordFragment == null){
            resetPasswordFragment = new ResetPasswordFragment();
        }
        return resetPasswordFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reset_password, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        firebaseAuth = FirebaseAuth.getInstance();

        initView(view);

        initListener();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initListener() {
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()){
                    String email = resetPasswordEmailEditText.getText().toString().trim();
                    firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(getContext(),"Please check your email account, If you want to reset your password",Toast.LENGTH_LONG).show();
                                Objects.requireNonNull(getActivity())
                                        .getSupportFragmentManager()
                                        .beginTransaction()
                                        .replace(R.id.loginContainer,SignInFragment.getInstance())
                                        .commit();
                            }
                            else {
                                String message = Objects.requireNonNull(task.getException()).getMessage();
                                Toast.makeText(getContext(),"Error Occurred "+message,Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });
    }

    private boolean checkValidation() {
        String email = resetPasswordEmailEditText.getText().toString().trim();
        if (email.isEmpty()){
            resetPasswordEmailEditText.setError(Constants.RESET_EMAIL_ERROR);
            return false;
        }
        return true;
    }

    private void initView(View view) {
        toolbar = view.findViewById(R.id.resetPasswordToolbar);
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(true);
        Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(true);
        Objects.requireNonNull(((AppCompatActivity)getActivity()).getSupportActionBar()).setTitle(Constants.RESET_PASSWORD);

        resetPasswordEmailEditText = view.findViewById(R.id.edtTextResetEmail);
        sendButton = view.findViewById(R.id.btnSendEmail);
    }
}
