package com.jagmit.android.postgram.Pojo;
/**
 * Created by MAYURESH NIMBALKAR on 18/1/19.
 */
public class Posts {
    private String uid;
    private String time;
    private String date;
    private String postimage;
    private String description;
    private String profileimage;
    private String fullname;

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setPostimage(String postimage) {
        this.postimage = postimage;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUid() {
        return uid;
    }

    public String getTime() {
        return time;
    }

    public String getDate() {
        return date;
    }

    public String getPostimage() {
        return postimage;
    }

    public String getDescription() {
        return description;
    }

    public String getProfileimage() {
        return profileimage;
    }

    public String getFullname() {
        return fullname;
    }
}
