package com.jagmit.android.postgram.AsyncTask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.jagmit.android.postgram.CallBacks.OnSignInCallBack;

import java.util.Objects;
/**
 * Created by MAYURESH NIMBALKAR on 18/1/19.
 */
public class SignInAsyncTask extends AsyncTask<String,String,Integer> {

    private String email, password;
    private FirebaseAuth firebaseAuth;
    private OnSignInCallBack onSignInCallBack;

    public SignInAsyncTask(String email, String password, OnSignInCallBack onSignInCallBack) {
        this.email = email;
        this.password = password;
        firebaseAuth = FirebaseAuth.getInstance();
        this.onSignInCallBack = onSignInCallBack;
    }

    @Override
    protected Integer doInBackground(String... strings) {
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    onSignInCallBack.onSuccess();
                }
                else {
                    String message = Objects.requireNonNull(task.getException()).getMessage().trim();
                    onSignInCallBack.onFailure(message);
                }
            }
        });
        return Integer.parseInt(String.valueOf(0));
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
    }
}
