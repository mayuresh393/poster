package com.jagmit.android.postgram.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.jagmit.android.postgram.Pojo.User;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.Constants;
import com.jagmit.android.postgram.Utils.FireBaseConstants;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class FindFriendsActivity extends AppCompatActivity {
    private ImageButton searchImageButton;
    private EditText searchEditText;
    private RecyclerView resultRecyclerView;
    private DatabaseReference allUserDatabaseRef;
    private FirebaseRecyclerAdapter<User,FindFriendsViewHolder> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);

        allUserDatabaseRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_USER);

        initView();

        initListener();

    }

    private void initListener() {
        searchImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String searchInput = searchEditText.getText().toString();
                searchPeopleAndFriends(searchInput);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void searchPeopleAndFriends(String searchInput) {

        Toast.makeText(FindFriendsActivity.this,"Searching......",Toast.LENGTH_LONG).show();

        Query searchFriendsQuery = allUserDatabaseRef.orderByChild(FireBaseConstants.FULL_NAME)
                .startAt(searchInput).endAt(searchInput+"\uf8ff");

        FirebaseRecyclerOptions<User> options = new FirebaseRecyclerOptions.Builder<User>()
                .setQuery(searchFriendsQuery,User.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<User, FindFriendsViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull FindFriendsViewHolder holder, final int position, @NonNull User model) {
                holder.bind(model);
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String visit_user_id =  getRef(position).getKey();
                        Intent friendProfileIntent = new Intent(FindFriendsActivity.this,FriendProfileActivity.class);
                        friendProfileIntent.putExtra(Constants.VISIT_USER_ID,visit_user_id);
                        startActivity(friendProfileIntent);
                    }
                });
            }

            @NonNull
            @Override
            public FindFriendsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                LayoutInflater layoutInflater = LayoutInflater.from(FindFriendsActivity.this);
                View view = layoutInflater.inflate(R.layout.all_user_display_layout,viewGroup,false);
                return new FindFriendsViewHolder(view);
            }
        };
        resultRecyclerView.setAdapter(adapter);
        adapter.startListening();
    }


    public static class FindFriendsViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView userCircleImageView;
        private TextView fullnameTextView,statusTextView;
        private View view;
        FindFriendsViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            userCircleImageView = itemView.findViewById(R.id.user_profile_image);
            fullnameTextView = itemView.findViewById(R.id.txtViewUserFullName);
            statusTextView = itemView.findViewById(R.id.txtViewUserStatus);
        }

        void bind(User model) {
            Picasso.get().load(model.getProfileimage()).into(userCircleImageView);
            fullnameTextView.setText(model.getFullname());
            statusTextView.setText(model.getStatus());
        }
    }

    private void initView() {
        Toolbar toolbar = findViewById(R.id.find_friends_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.FIND_FRIENDS);


        searchEditText = findViewById(R.id.searchEdtText);
        searchImageButton = findViewById(R.id.imageButtonSearch);
        resultRecyclerView = findViewById(R.id.recyclerViewSearchList);

        resultRecyclerView.setHasFixedSize(true);
        resultRecyclerView.setLayoutManager(new LinearLayoutManager(FindFriendsActivity.this));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                sendUserToHomeActivity();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendUserToHomeActivity() {
        Intent homeIntent = new Intent(FindFriendsActivity.this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(homeIntent);
        finish();
    }
}
