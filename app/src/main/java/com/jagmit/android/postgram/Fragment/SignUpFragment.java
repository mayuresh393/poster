package com.jagmit.android.postgram.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.jagmit.android.postgram.Activity.HomeActivity;
import com.jagmit.android.postgram.Activity.SetupActivity;
import com.jagmit.android.postgram.CallBacks.OnRegisterCallBack;
import com.jagmit.android.postgram.Utils.Constants;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.AsyncTask.RegisterAsyncTask;

import java.util.Objects;

public class SignUpFragment extends Fragment {
    private static SignUpFragment signUpFragment = null;
    private TextView textViewHintSignIn,textViewSignIn;
    private EditText editTextEmail,editTextPassword,editTextConfirmPassword;
    private Button buttonSignUp;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    public static SignUpFragment getInstance() {
        if (signUpFragment == null){
            signUpFragment = new SignUpFragment();
        }
        return signUpFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        firebaseAuth = FirebaseAuth.getInstance();
        initView(view);

        initViewListeners();

    }

    private void initViewListeners() {
        textViewHintSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textViewClick();
            }
        });

        textViewSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textViewClick();
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkEmptyValidation()){
                    progressDialog = new ProgressDialog(getContext());
                    progressDialog.setTitle("Please wait while registering ..");
                    progressDialog.setCancelable(false);
                    progressDialog.setIndeterminate(true);
                    progressDialog.show();
                    registerUser();
                }
            }
        });
    }


    private void registerUser() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        RegisterAsyncTask registerAsyncTask = new RegisterAsyncTask(email,password, new OnRegisterCallBack() {
            @Override
            public void onSuccess() {
                Toast.makeText(getContext(),"You are registered successfully",Toast.LENGTH_SHORT).show();
                sendUserToSetupActivity();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(String s) {
                Toast.makeText(getContext(),"Error occurred while registering "+s,Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
        registerAsyncTask.execute();
    }

    private void sendUserToSetupActivity() {
        Intent setupIntent = new Intent(getContext(),SetupActivity.class);
        setupIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(setupIntent);
    }

    private boolean checkEmptyValidation() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        String confirmPassword = editTextConfirmPassword.getText().toString().trim();

        if (email.isEmpty()){
            editTextEmail.setError(Constants.EMAIL_ERROR);
            return false;
        }

        if (password.isEmpty()){
            editTextPassword.setError(Constants.PASSWORD_ERROR);
            return false;
        }
        if (confirmPassword.isEmpty()){
            editTextConfirmPassword.setError(Constants.CONFIRM_PASSWORD_ERROR);
            return false;
        }

        if (!passwordValidation(password,confirmPassword)){
            editTextConfirmPassword.setError(Constants.MATCH_PASSWORD_ERROR);
            return false;
        }

        return true;
    }

    private boolean passwordValidation(String password, String confirmPassword) {
        return password.equals(confirmPassword);
    }

    private void initView(View view) {
        textViewHintSignIn = view.findViewById(R.id.txtViewHintSignIn);
        textViewSignIn = view.findViewById(R.id.txtViewSignIn);
        editTextEmail = view.findViewById(R.id.edtTextEmail);
        editTextPassword = view.findViewById(R.id.edtTextPassword);
        editTextConfirmPassword = view.findViewById(R.id.edtTextConfirmPassword);
        buttonSignUp = view.findViewById(R.id.btnSignUp);
    }

    private void textViewClick() {
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.loginContainer,SignInFragment.getInstance())
                .commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

        if (firebaseUser != null){
            sendUserToHomeActivity();
        }
    }

    private void sendUserToHomeActivity() {
        Intent homeIntent = new Intent(getActivity(),HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(homeIntent);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
