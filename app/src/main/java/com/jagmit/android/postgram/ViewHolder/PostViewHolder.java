package com.jagmit.android.postgram.ViewHolder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jagmit.android.postgram.CallBacks.OnPostClickCallBack;
import com.jagmit.android.postgram.Pojo.Posts;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.FireBaseConstants;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
/**
 * Created by MAYURESH NIMBALKAR on 18/1/19.
 */
public class PostViewHolder extends RecyclerView.ViewHolder {
    private CircleImageView profileCircleImageView;
    private TextView usernameTextView,dateTextView,timeTextView,descriptionTextView;
    private ImageView postImageView;
    public ImageButton likeImageButton,commentImageButton;
    private TextView likeNoTextView;
    private OnPostClickCallBack onPostClickCallBack;
    private int countLikes;
    private String currentUserId;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference likeRef;

    public PostViewHolder(@NonNull View itemView) {
        super(itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPostClickCallBack.onClick();
            }
        });

        likeImageButton = itemView.findViewById(R.id.imageBtnLike);
        commentImageButton = itemView.findViewById(R.id.imageBtnComment);
        likeNoTextView = itemView.findViewById(R.id.txtViewLikes);
        profileCircleImageView = itemView.findViewById(R.id.postProfile);
        usernameTextView = itemView.findViewById(R.id.postUsername);
        dateTextView = itemView.findViewById(R.id.postDate);
        timeTextView = itemView.findViewById(R.id.postTime);
        descriptionTextView = itemView.findViewById(R.id.postDescription);
        postImageView = itemView.findViewById(R.id.postImage);

        firebaseAuth = FirebaseAuth.getInstance();
        currentUserId = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        likeRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_LIKE);
    }

    public void bind(Posts posts) {
        Picasso.get().load(posts.getProfileimage()).into(profileCircleImageView);
        Picasso.get().load(posts.getPostimage()).into(postImageView);
        usernameTextView.setText(posts.getFullname().trim());
        dateTextView.setText("    "+posts.getDate().trim());
        timeTextView.setText("    "+posts.getTime().trim());
        descriptionTextView.setText(posts.getDescription().trim());
    }

    public void seOnClickListener(OnPostClickCallBack onPostClickCallBack) {
        this.onPostClickCallBack = onPostClickCallBack;
    }

    public void setLikesButtonStatus(final String postKey) {
        likeRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(postKey).hasChild(currentUserId)){
                    countLikes = (int) dataSnapshot.child(postKey).getChildrenCount();
                    likeImageButton.setImageResource(R.drawable.ic_icon_filled_like);
                    likeNoTextView.setText(String.valueOf(countLikes));
                }
                else {
                    countLikes = (int) dataSnapshot.child(postKey).getChildrenCount();
                    likeImageButton.setImageResource(R.drawable.ic_icon_unfill_like);
                    likeNoTextView.setText(String.valueOf(countLikes));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
