package com.jagmit.android.postgram.Adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jagmit.android.postgram.Pojo.Message;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.Constants;
import com.jagmit.android.postgram.Utils.FireBaseConstants;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by MAYURESH NIMBALKAR on 24/3/19.
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
    private List<Message>userMessageList;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference userReference;

    public MessageAdapter(List<Message> userMessageList) {
        this.userMessageList = userMessageList;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.message_layout,viewGroup,false);
        firebaseAuth = FirebaseAuth.getInstance();
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MessageViewHolder messageViewHolder, int i) {
        String messageSenderId = firebaseAuth.getCurrentUser().getUid();
        Message message = userMessageList.get(i);

        String fromUserId = message.getFrom();
        String fromMessageType = message.getType();

        userReference = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_USER).child(fromUserId);
        userReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){

                    String image = dataSnapshot.child(FireBaseConstants.PROFILE_IMAGE).getValue().toString();
                    Picasso.get().load(image).placeholder(R.drawable.ic_logo_profile).into(messageViewHolder.receiveProfileImage);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        if (fromMessageType.equals("text")){
            messageViewHolder.receiverMessageText.setVisibility(View.INVISIBLE);
            messageViewHolder.receiveProfileImage.setVisibility(View.INVISIBLE);

            if (fromUserId.equals(messageSenderId)){
                messageViewHolder.senderMessageText.setBackgroundResource(R.drawable.sender_message_text_background);
                messageViewHolder.senderMessageText.setTextColor(Color.WHITE);
                messageViewHolder.senderMessageText.setGravity(Gravity.LEFT);
                messageViewHolder.senderMessageText.setText(message.getMessage());
            }else {
                messageViewHolder.senderMessageText.setVisibility(View.INVISIBLE);

                messageViewHolder.receiverMessageText.setVisibility(View.VISIBLE);
                messageViewHolder.receiveProfileImage.setVisibility(View.VISIBLE);

                messageViewHolder.receiverMessageText.setBackgroundResource(R.drawable.receiver_message_text_background);
                messageViewHolder.receiverMessageText.setTextColor(Color.WHITE);
                messageViewHolder.receiverMessageText.setGravity(Gravity.LEFT);
                messageViewHolder.receiverMessageText.setText(message.getMessage());
            }
        }
    }

    @Override
    public int getItemCount() {
        return userMessageList.size();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder{
        private TextView senderMessageText,receiverMessageText;
        private CircleImageView receiveProfileImage;
        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);

            senderMessageText = itemView.findViewById(R.id.senderMessageText);
            receiverMessageText = itemView.findViewById(R.id.receiverMessageText);
            receiveProfileImage = itemView.findViewById(R.id.messageProfileLogo);
        }
    }
}
