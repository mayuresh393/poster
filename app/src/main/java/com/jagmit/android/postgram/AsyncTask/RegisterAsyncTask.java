package com.jagmit.android.postgram.AsyncTask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.jagmit.android.postgram.CallBacks.OnRegisterCallBack;

import java.util.Objects;
/**
 * Created by MAYURESH NIMBALKAR on 18/1/19.
 */
public class RegisterAsyncTask extends AsyncTask<String, String, Integer> {

    private String email, password;
    private FirebaseAuth firebaseAuth;
    private OnRegisterCallBack onRegisterCallBack;

    public RegisterAsyncTask(String email, String password, OnRegisterCallBack onRegisterCallBack) {
        this.email = email;
        this.password = password;
        this.onRegisterCallBack = onRegisterCallBack;
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected Integer doInBackground(String... strings) {
        return processing();
    }

    private Integer processing() {
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    onRegisterCallBack.onSuccess();

                } else {
                    String message = Objects.requireNonNull(task.getException()).getMessage().trim();
                    onRegisterCallBack.onFailure(message);
                }
            }
        });
        return Integer.parseInt(String.valueOf(0));
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
    }
}
