package com.jagmit.android.postgram.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.jagmit.android.postgram.CallBacks.OnPostClickCallBack;
import com.jagmit.android.postgram.Utils.Constants;
import com.jagmit.android.postgram.Pojo.Posts;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.FireBaseConstants;
import com.jagmit.android.postgram.ViewHolder.PostViewHolder;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity {
    private NavigationView navigationView;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private RecyclerView recyclerView;
    private CircleImageView circleImageViewProfile;
    private TextView textViewProfileName;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference,postRef,likesRef;
    private String currentUserID;
    private FirebaseUser firebaseUser;
    private boolean likeCheck;
    private FirebaseRecyclerAdapter<Posts,PostViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        firebaseAuth = FirebaseAuth.getInstance();

        firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser != null) {
            currentUserID = firebaseAuth.getCurrentUser().getUid();
        }

        databaseReference = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_USER);
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Profile Images");
        postRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_POSTS);
        likesRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_LIKE);

        initView();

        initViewListeners();
    }

    private void initViewListeners() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                userMenuSelector(menuItem);
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

        if (firebaseUser == null){
            sendUserToLoginActivity();
        }
        else{
            checkUserExistenceInDatabase();
        }
        adapter.startListening();
    }

    private void checkUserExistenceInDatabase() {
        final String current_userId = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild(current_userId)){
                    sendUserToSetUpActivity();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void sendUserToSetUpActivity() {
        Intent setupIntent = new Intent(HomeActivity.this,SetupActivity.class);
        setupIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(setupIntent);
        finish();
    }

    private void sendUserToLoginActivity() {

        Intent loginIntent = new Intent(HomeActivity.this,LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        finish();
    }

    private void userMenuSelector(MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.nav_profile:{
                sendUserToProfileActivity();
                break;
            }
            case R.id.nav_home:{
                break;
            }
            case R.id.nav_find_friends:{
                sendUserToFindFriendsActivity();
                break;
            }
            case R.id.nav_friends:{
                sendUserToFriendsActivity();
                break;
            }
            case R.id.nav_messages:{
                sendUserToFriendsActivity();
                break;
            }
            case R.id.nav_post:{
                sendUserToPostActivity();
                break;
            }
            case R.id.nav_logout:{
                firebaseAuth.signOut();
                sendUserToLoginActivity();
                break;
            }
            case R.id.nav_settings:{
                sendUserToSettingsActivity();
                break;
            }
        }
    }

    private void sendUserToFriendsActivity() {
        Intent friendIntent = new Intent(HomeActivity.this,FriendsActivity.class);
        startActivity(friendIntent);
    }

    private void sendUserToFindFriendsActivity() {
        Intent findFriendsIntent = new Intent(HomeActivity.this,FindFriendsActivity.class);
        startActivity(findFriendsIntent);
    }

    private void sendUserToProfileActivity() {
        Intent profileIntent = new Intent(HomeActivity.this,ProfileActivity.class);
        startActivity(profileIntent);
    }

    private void sendUserToSettingsActivity() {
        Intent settingsIntent = new Intent(HomeActivity.this,SettingsActivity.class);
        startActivity(settingsIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.addPost){
            sendUserToPostActivity();
        }
        if (actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendUserToPostActivity() {
        Intent postIntent = new Intent(HomeActivity.this,PostActivity.class);
        startActivity(postIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.corner_menu,menu);
        return true;
    }


    private void initView() {
        Toolbar toolbar = findViewById(R.id.home_toolbar);

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(Constants.HOME);

        DrawerLayout drawerLayout = findViewById(R.id.drawerLayout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(HomeActivity.this, drawerLayout,R.string.drawer_open,R.string.drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigationView = findViewById(R.id.navigationView);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeActivity.this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        View navView = navigationView.inflateHeaderView(R.layout.navigation_header_resource_file);

        circleImageViewProfile = navView.findViewById(R.id.circleImageView);
        textViewProfileName = navView.findViewById(R.id.txtViewUsername);

        loadProfile();

        displayAllUsersPost();

    }

    private void displayAllUsersPost() {

        Query sortPostUpdated = postRef.orderByChild(FireBaseConstants.COUNTER);

        FirebaseRecyclerOptions<Posts> options = new FirebaseRecyclerOptions.Builder<Posts>()
                .setQuery(sortPostUpdated,Posts.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<Posts, PostViewHolder>(options) {
            @NonNull
            @Override
            public PostViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
                View view = layoutInflater.inflate(R.layout.all_post_layout,viewGroup,false);
                return new PostViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull PostViewHolder holder, int position, @NonNull Posts model) {
                final int viewPosition = position;
                final String postKey = getRef(viewPosition).getKey();
                holder.seOnClickListener(new OnPostClickCallBack() {
                    @Override
                    public void onClick() {
                        Intent postViewIntent = new Intent(HomeActivity.this,PostViewActivity.class);
                        postViewIntent.putExtra(Constants.POST_KEY, postKey);
                        startActivity(postViewIntent);
                    }
                });

                holder.bind(model);

                holder.commentImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent commentsIntent = new Intent(HomeActivity.this,CommentsActivity.class);
                        commentsIntent.putExtra(Constants.POST_KEY, postKey);
                        startActivity(commentsIntent);
                    }
                });

                holder.likeImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        likeCheck = true;

                        likesRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (likeCheck){
                                    if (postKey != null) {
                                        if (dataSnapshot.child(postKey).hasChild(currentUserID)){
                                            likesRef.child(postKey).child(currentUserID).removeValue();
                                            likeCheck = false;
                                        }
                                        else {
                                            likesRef.child(postKey).child(currentUserID).setValue(true);
                                            likeCheck = false;
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                });

                holder.setLikesButtonStatus(postKey);
            }
        };
        recyclerView.setAdapter(adapter);
        adapter.startListening();
    }

    private void loadProfile() {
        if (firebaseUser != null) {
            databaseReference.child(currentUserID).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        if (dataSnapshot.hasChild(FireBaseConstants.FULL_NAME))
                        {
                            String fullname = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.FULL_NAME).getValue()).toString();
                            textViewProfileName.setText(fullname);
                        }
                        if (dataSnapshot.hasChild(FireBaseConstants.PROFILE_IMAGE)){
                            String image = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.PROFILE_IMAGE).getValue()).toString();
                            Picasso.get().load(image).placeholder(R.drawable.ic_logo_profile).into(circleImageViewProfile);
                        }
                        else {
                            Toast.makeText(HomeActivity.this,"Profile Image does not exist",Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }
}
