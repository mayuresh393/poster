package com.jagmit.android.postgram.Activity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jagmit.android.postgram.Adapter.MessageAdapter;
import com.jagmit.android.postgram.Pojo.Message;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.Constants;
import com.jagmit.android.postgram.Utils.FireBaseConstants;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageButton sendImageButton,fileUploadImageButton;
    private EditText messageEditText;
    private RecyclerView messageListRecyclerView;
    private List<Message> messageList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MessageAdapter messageAdapter;
    private String messageReceiverId, messageReceiverName,messageSenderId,currentDate,currentTime;
    private DatabaseReference rootReference;
    private FirebaseAuth firebaseAuth;
    private TextView receiverUsername;

    private CircleImageView receiverProfileCircleImageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        messageReceiverId = Objects.requireNonNull(Objects.requireNonNull(getIntent().getExtras()).get(Constants.VISIT_USER_ID)).toString();
        messageReceiverName = Objects.requireNonNull(Objects.requireNonNull(getIntent().getExtras()).get(Constants.USERNAME)).toString();

        firebaseAuth = FirebaseAuth.getInstance();
        messageSenderId = firebaseAuth.getCurrentUser().getUid();
        rootReference = FirebaseDatabase.getInstance().getReference();
        initUI();

        displayReceiverInfo();

        setListeners();
    }

    private void setListeners() {
        sendImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateMessage()){
                    sendMessage();
                }
            }
        });

        fetchMessages();
    }

    private void fetchMessages() {
        rootReference.child(FireBaseConstants.NODE_MESSAGE).child(messageSenderId).child(messageReceiverId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if (dataSnapshot.exists()){
                    Message message = dataSnapshot.getValue(Message.class);
                    messageList.add(message);
                    messageAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private boolean validateMessage() {
        String message = messageEditText.getText().toString().trim();
        if (message.isEmpty()){
            messageEditText.setError(Constants.MESSAGE_ERROR);
            return false;
        }
        else {
            return true;
        }
    }

    private void sendMessage() {
        String message = messageEditText.getText().toString().trim();

        String message_sender_ref = FireBaseConstants.NODE_MESSAGE+"/"+messageSenderId+"/"+messageReceiverId;
        String message_receiver_ref = FireBaseConstants.NODE_MESSAGE+"/"+messageReceiverId+"/"+messageSenderId;

        DatabaseReference usersMessageKeyReference = rootReference.child(FireBaseConstants.NODE_MESSAGE).child(messageSenderId)
                .child(messageReceiverId).push();

        String messagePushId = usersMessageKeyReference.getKey();

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
        currentDate = simpleDateFormat.format(calendar.getTime());

        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm aa");
        currentTime = simpleTimeFormat.format(calendar.getTime());

        Map messageTextBody = new HashMap();
        messageTextBody.put(FireBaseConstants.MESSAGE,message);
        messageTextBody.put(FireBaseConstants.TIME,currentTime);
        messageTextBody.put(FireBaseConstants.DATE,currentDate);
        messageTextBody.put(FireBaseConstants.TYPE,"text");
        messageTextBody.put(FireBaseConstants.FROM,messageSenderId);

        Map messageBodyDetail = new HashMap();
        messageBodyDetail.put(message_sender_ref+ "/" +messagePushId, messageTextBody);
        messageBodyDetail.put(message_receiver_ref+ "/" +messagePushId, messageTextBody);

        rootReference.updateChildren(messageBodyDetail).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()){
                    Toast.makeText(ChatActivity.this,"Message Send Successfully",Toast.LENGTH_SHORT).show();
                    messageEditText.setText("");
                }else {
                    String message = task.getException().getMessage();
                    Toast.makeText(ChatActivity.this,"Error Occurred:"+message,Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void displayReceiverInfo() {
        receiverUsername.setText(messageReceiverName);

        rootReference.child(FireBaseConstants.NODE_USER).child(messageReceiverId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    final  String profileImage = dataSnapshot.child(FireBaseConstants.PROFILE_IMAGE).getValue().toString();
                    Picasso.get().load(profileImage).placeholder(R.drawable.ic_logo_profile).into(receiverProfileCircleImageView);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initUI() {
        toolbar =  findViewById(R.id.chat_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View actionBarView = layoutInflater.inflate(R.layout.chat_custom_bar,null);
        actionBar.setCustomView(actionBarView);

        receiverUsername = findViewById(R.id.customProfileName);
        receiverProfileCircleImageView = findViewById(R.id.customProfileImage);

        sendImageButton = findViewById(R.id.imageBtnSend);
        fileUploadImageButton = findViewById(R.id.imageBtnUpload);
        messageEditText = findViewById(R.id.EditTextChat);

        messageAdapter = new MessageAdapter(messageList);
        messageListRecyclerView = findViewById(R.id.rvMessages);
        linearLayoutManager = new LinearLayoutManager(this);
        messageListRecyclerView.setHasFixedSize(true);
        messageListRecyclerView.setLayoutManager(linearLayoutManager);

        messageListRecyclerView.setAdapter(messageAdapter);
    }
}
