package com.jagmit.android.postgram.Utils;

/**
 * Created by MAYURESH NIMBALKAR on 19/1/19.
 */
public interface FireBaseConstants {

    String PROFILE_IMAGE = "profileimage";
    String USER_NAME = "username";
    String FULL_NAME = "fullname";
    String GENDER = "gender";
    String STATUS = "status";
    String COUNTRY = "country";
    String DOB = "dob";
    String RELATION_STATUS = "relationshipstatus";
    String UID = "uid";
    String DATE = "date";
    String TIME = "time";
    String NODE_FRIENDS = "Friends";
    String NODE_FRIEND_REQUEST = "FriendsRequests";
    String NODE_LIKE = "Likes";
    String NODE_MESSAGE = "Messages";
    String NODE_POSTS = "Posts";
    String NODE_USER = "Users";
    String DESCRIPTION = "description";
    String POST_IMAGE = "postimage";
    String COMMENTS = "comments";
    String COUNTER = "counter";
    String MESSAGE = "message";
    String TYPE = "type";
    String FROM = "from";
    String NODE_COMMENTS = "Comments";
}

