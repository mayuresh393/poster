package com.jagmit.android.postgram.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.jagmit.android.postgram.Activity.HomeActivity;
import com.jagmit.android.postgram.AsyncTask.SignInAsyncTask;
import com.jagmit.android.postgram.CallBacks.OnSignInCallBack;
import com.jagmit.android.postgram.Utils.Constants;
import com.jagmit.android.postgram.R;

import java.util.Objects;

public class SignInFragment extends Fragment {
    private static SignInFragment signInFragment = null;
    private TextView textViewHintSignUp,textViewSignUp,forgetPasswordTextView;
    private Button buttonSignIn;
    private EditText editTextEmail, editTextPassword;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    public static SignInFragment getInstance() {
        if (signInFragment == null){
            signInFragment = new SignInFragment();
        }
        return signInFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firebaseAuth = FirebaseAuth.getInstance();

        initView(view);

        initViewListeners();

    }

    private void initViewListeners() {
        textViewHintSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textViewClick();
            }
        });

        textViewSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textViewClick();
            }
        });

        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()){
                    progressDialog = new ProgressDialog(getContext());
                    progressDialog.setTitle("Please wait while logging in.. ");
                    progressDialog.setCancelable(false);
                    progressDialog.setIndeterminate(true);
                    progressDialog.show();
                    allowingUserToLogin();
                }
            }
        });

        forgetPasswordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Objects.requireNonNull(getActivity())
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.loginContainer,ResetPasswordFragment.getInstance())
                        .commit();
            }
        });
    }

    private boolean checkValidation() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        if (email.isEmpty()){
            editTextEmail.setError(Constants.EMAIL_ERROR);
            return false;
        }
        if (password.isEmpty()){
            editTextPassword.setError(Constants.PASSWORD_ERROR);
            return false;
        }

        return true;
    }

    private void allowingUserToLogin() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        SignInAsyncTask signInAsyncTask = new SignInAsyncTask(email,password, new OnSignInCallBack() {
            @Override
            public void onSuccess() {
                sendUserToHomeActivity();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(String message) {
                Toast.makeText(getContext(),"Error Occurred While Login "+message,Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
        signInAsyncTask.execute();
    }

    private void sendUserToHomeActivity() {
        Intent homeIntent = new Intent(getContext(),HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(homeIntent);
    }

    private void initView(View view) {
        textViewHintSignUp = view.findViewById(R.id.txtViewHintSignUp);
        textViewSignUp = view.findViewById(R.id.txtViewSignUp);
        forgetPasswordTextView = view.findViewById(R.id.txtViewForgotPasswordLink);

        editTextEmail = view.findViewById(R.id.edtTextEmail);
        editTextPassword = view.findViewById(R.id.edtTextPassword);

        buttonSignIn = view.findViewById(R.id.btnSignIn);
    }

    private void textViewClick() {
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.loginContainer,SignUpFragment.getInstance())
                .commit();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

        if (firebaseUser != null){
            sendUserToHomeActivity();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
