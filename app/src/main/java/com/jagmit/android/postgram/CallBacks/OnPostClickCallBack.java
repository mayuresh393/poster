package com.jagmit.android.postgram.CallBacks;

/**
 * Created by MAYURESH NIMBALKAR on 19/1/19.
 */
public interface OnPostClickCallBack {
    void onClick();
}
