package com.jagmit.android.postgram.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jagmit.android.postgram.Pojo.Friend;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.Constants;
import com.jagmit.android.postgram.Utils.FireBaseConstants;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendsActivity extends AppCompatActivity {

    private RecyclerView recyclerViewFriendList;
    private DatabaseReference friendReference,userReference;
    private FirebaseAuth firebaseAuth;
    private String onlineUserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        firebaseAuth = FirebaseAuth.getInstance();
        onlineUserID = firebaseAuth.getCurrentUser().getUid();
        friendReference = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_FRIENDS).child(onlineUserID);
        userReference = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_USER);
        initUI();


        displayAllFriends();
    }

    private void initUI() {
        recyclerViewFriendList = findViewById(R.id.rvFriendList);
        recyclerViewFriendList.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        recyclerViewFriendList.setLayoutManager(linearLayoutManager);
    }

    private void displayAllFriends() {
        FirebaseRecyclerOptions<Friend> options = new FirebaseRecyclerOptions.Builder<Friend>()
                .setQuery(friendReference,Friend.class)
                .build();

        FirebaseRecyclerAdapter<Friend,FriendsViewHolder> adapter = new FirebaseRecyclerAdapter<Friend, FriendsViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final FriendsViewHolder holder, int position, @NonNull final Friend model) {
                final String userIds = getRef(position).getKey();
                model.setDate(model.getDate());
                userReference.child(userIds).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            final String username = dataSnapshot.child(FireBaseConstants.FULL_NAME).getValue().toString();
                            final String profileImage = dataSnapshot.child(FireBaseConstants.PROFILE_IMAGE).getValue().toString();


                            model.setFullname(username);
                            model.setProfileimage(profileImage);

                            holder.bind(model);
                            holder.view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CharSequence options[] = new CharSequence[]{
                                            username + "'s Profile",
                                            "Send Message"
                                    };
                                    AlertDialog.Builder builder = new AlertDialog.Builder(FriendsActivity.this);
                                    builder.setTitle("Select Option");
                                    builder.setItems(options, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (which == 0){
                                                sendUserToFriendProfile(userIds);
                                            }
                                            else if (which == 1){
                                                sendUserToChatActivity(userIds,username);
                                            }
                                        }
                                    });
                                    builder.show();
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                holder.bind(model);
            }

            @NonNull
            @Override
            public FriendsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                LayoutInflater layoutInflater = LayoutInflater.from(FriendsActivity.this);
                View view = layoutInflater.inflate(R.layout.all_friend_display_layout,viewGroup,false);
                return new FriendsActivity.FriendsViewHolder(view);
            }
        };
        recyclerViewFriendList.setAdapter(adapter);
        adapter.startListening();
    }

    private void sendUserToChatActivity(String userIds, String username) {
        Intent chatIntent = new Intent(FriendsActivity.this,ChatActivity.class);
        chatIntent.putExtra(Constants.VISIT_USER_ID,userIds);
        chatIntent.putExtra(Constants.USERNAME,username);
        startActivity(chatIntent);
    }

    private void sendUserToFriendProfile(String userIds) {
        Intent profileIntent = new Intent(FriendsActivity.this,FriendProfileActivity.class);
        profileIntent.putExtra(Constants.VISIT_USER_ID,userIds);
        startActivity(profileIntent);
    }

    public static class FriendsViewHolder extends RecyclerView.ViewHolder{
        private View view;
        private CircleImageView userCircleImageView;
        private TextView fullnameTextView, dateTextView;
        public FriendsViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            userCircleImageView = itemView.findViewById(R.id.friend_profile_image);
            fullnameTextView = itemView.findViewById(R.id.txtViewFriendFullName);
            dateTextView = itemView.findViewById(R.id.txtViewFriendDate);
        }

        void bind(Friend friend) {
            Picasso.get().load(friend.getProfileimage()).into(userCircleImageView);
            fullnameTextView.setText(friend.getFullname());
            dateTextView.setText("Friend Since : "+friend.getDate());
        }
    }
}
