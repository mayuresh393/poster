package com.jagmit.android.postgram.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jagmit.android.postgram.Pojo.Comment;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.Constants;
import com.jagmit.android.postgram.Utils.FireBaseConstants;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;

public class CommentsActivity extends AppCompatActivity {
    private ImageButton sendImageButton;
    private EditText commentEditText;
    private RecyclerView commentsRecyclerView;
    private String postKey;
    private DatabaseReference usersRef,postRef;
    private String currentUserId;
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        postKey = Objects.requireNonNull(Objects.requireNonNull(getIntent().getExtras()).get(Constants.POST_KEY)).toString();
        firebaseAuth = FirebaseAuth.getInstance();
        currentUserId = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        usersRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_USER);
        postRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_POSTS).child(postKey).child(FireBaseConstants.NODE_COMMENTS);
        initView();

        initListeners();
    }

    private void initListeners() {
        sendImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateComments()){
                    usersRef.child(currentUserId).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()){
                                String username = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.USER_NAME).getValue()).toString();

                                storingCommentsToFirebaseStorage(username);

                                commentEditText.setText("");
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }
        });
    }

    private void storingCommentsToFirebaseStorage(String username) {
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
        final String currentDate = simpleDateFormat.format(calendar.getTime());

        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm");
        final String currentTime = simpleTimeFormat.format(calendar.getTime());

        final String randomKey = currentUserId + currentDate + currentTime;

        HashMap<String, Object> commentsMap = new HashMap<>();
        commentsMap.put(FireBaseConstants.UID,currentUserId);
        commentsMap.put(FireBaseConstants.COMMENTS,commentEditText.getText().toString().trim());
        commentsMap.put(FireBaseConstants.DATE,currentDate);
        commentsMap.put(FireBaseConstants.TIME,currentTime);
        commentsMap.put(FireBaseConstants.USER_NAME,username);

        postRef.child(randomKey).updateChildren(commentsMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(CommentsActivity.this,"You have commented successfully",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(CommentsActivity.this,"Error Occurred while commenting "+ Objects.requireNonNull(task.getException()).getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public static class CommentViewHolder extends RecyclerView.ViewHolder{
        private TextView usernameTextView,dateTextView,timeTextView,commentTextView;
        CommentViewHolder(@NonNull View itemView) {
            super(itemView);
            usernameTextView = itemView.findViewById(R.id.txtViewCommentUserName);
            dateTextView = itemView.findViewById(R.id.txtViewCommentDate);
            timeTextView = itemView.findViewById(R.id.txtViewCommentTime);
            commentTextView = itemView.findViewById(R.id.txtViewCommentText);
        }

        void bind(Comment model) {
            usernameTextView.setText("@"+model.getUsername()+" ");
            dateTextView.setText("Date: "+model.getDate());
            timeTextView.setText("Time: "+model.getTime());
            commentTextView.setText(model.getComments());

        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseRecyclerOptions<Comment> options = new FirebaseRecyclerOptions.Builder<Comment>()
                .setQuery(postRef,Comment.class)
                .build();

        FirebaseRecyclerAdapter<Comment,CommentViewHolder> adapter = new FirebaseRecyclerAdapter<Comment, CommentViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull CommentViewHolder holder, int position, @NonNull Comment model) {
                holder.bind(model);
            }

            @NonNull
            @Override
            public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                LayoutInflater layoutInflater = LayoutInflater.from(CommentsActivity.this);
                View view = layoutInflater.inflate(R.layout.all_user_comments_layout,viewGroup,false);
                return new CommentViewHolder(view);
            }
        };
        commentsRecyclerView.setAdapter(adapter);
        adapter.startListening();
    }

    private boolean validateComments() {

        String comments = commentEditText.getText().toString().trim();
        if (comments.isEmpty()){
            commentEditText.setError(Constants.COMMENT_ERROR);
            return false;
        }
        return true;
    }

    private void initView() {

        commentsRecyclerView = findViewById(R.id.recyclerViewComments);
        commentsRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CommentsActivity.this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        commentsRecyclerView.setLayoutManager(linearLayoutManager);

        commentEditText = findViewById(R.id.EditTextComments);
        sendImageButton = findViewById(R.id.imageBtnSend);

    }
}
