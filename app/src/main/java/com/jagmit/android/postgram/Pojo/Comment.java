package com.jagmit.android.postgram.Pojo;

/**
 * Created by MAYURESH NIMBALKAR on 20/1/19.
 */
public class Comment {

    private String username;
    private String date;
    private String time;
    private String comments;
    private String uid;

    public void setUsername(String username) {
        this.username = username;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getComments() {
        return comments;
    }

    public String getUid() {
        return uid;
    }
}
