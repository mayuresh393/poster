package com.jagmit.android.postgram.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.jagmit.android.postgram.Fragment.SignInFragment;
import com.jagmit.android.postgram.R;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loadFragment(SignInFragment.getInstance());
    }

    private void loadFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.loginContainer, fragment)
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
