package com.jagmit.android.postgram.CallBacks;
/**
 * Created by MAYURESH NIMBALKAR on 18/1/19.
 */
public interface OnSignInCallBack {
    void onSuccess();
    void onFailure(String message);
}
