package com.jagmit.android.postgram.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.FireBaseConstants;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {
    private TextView usernameTextView, userProfNameTextView, userStatusTextView, userCountryTextView, userGenderTextView, userRelationshipStatusTextView, userDobTextView;
    private CircleImageView userProfileCircleImageView;
    private DatabaseReference profileUserRef;
    private FirebaseAuth firebaseAuth;
    private String currentUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        firebaseAuth = FirebaseAuth.getInstance();
        currentUserId = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        profileUserRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_USER).child(currentUserId);

        initView();

        initListeners();
    }

    private void initListeners() {
        profileUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String profileImage = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.PROFILE_IMAGE).getValue()).toString();
                    String username = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.USER_NAME).getValue()).toString();
                    String fullname = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.FULL_NAME).getValue()).toString();
                    String gender = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.GENDER).getValue()).toString();
                    String status = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.STATUS).getValue()).toString();
                    String country = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.COUNTRY).getValue()).toString();
                    String dob = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.DOB).getValue()).toString();
                    String relationshipStatus = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.RELATION_STATUS).getValue()).toString();


                    Picasso.get().load(profileImage).placeholder(R.drawable.ic_logo_profile).into(userProfileCircleImageView);

                    usernameTextView.setText("@" + username);
                    userProfNameTextView.setText(fullname);
                    userGenderTextView.setText("Gender: " + gender);
                    userStatusTextView.setText(status);
                    userCountryTextView.setText("Country: " + country);
                    userDobTextView.setText("DOB: " + dob);
                    userRelationshipStatusTextView.setText("Relationship Status: " + relationshipStatus);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initView() {
        usernameTextView = findViewById(R.id.profile_username);
        userProfNameTextView = findViewById(R.id.profile_name);
        userCountryTextView = findViewById(R.id.profile_country);
        userDobTextView = findViewById(R.id.profile_dob);
        userStatusTextView = findViewById(R.id.profile_status);
        userRelationshipStatusTextView = findViewById(R.id.profile_relation_status);
        userGenderTextView = findViewById(R.id.profile_gender);

        userProfileCircleImageView = findViewById(R.id.profile_pic);
    }
}
