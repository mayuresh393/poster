package com.jagmit.android.postgram.Utils;
/**
 * Created by MAYURESH NIMBALKAR on 18/1/19.
 */
public interface Constants {
    String EMAIL_ERROR = "Email field could not be empty";
    String PASSWORD_ERROR = "Password field Could not be empty";
    String CONFIRM_PASSWORD_ERROR = "Confirm Password field Could not be empty";
    String MATCH_PASSWORD_ERROR = "Password could not match with confirm password";
    String USERNAME_ERROR = "Username field could not be empty";
    String FULLNAME_ERROR = "Full Name field could not be blank";
    String COUNTRY_ERROR = "Country name field could not be blank";
    String GENDER_ERROR = "Please Select Gender";
    String DEFAULT_STATUS = "hey there, I am using Postgram Social Network";
    String DEFAULT_NONE_VALUE = "none";
    String UPDATE_POST = "Update Post";
    String POST_ERROR = "Please write something about Post";
    String POST_KEY = "postKey";
    String DELETE_POST_MESSAGE = "Post has been deleted successfully";
    String BUILDER_TITLE = "Edit Post";
    String ACCOUNT_SETTINGS = "Account Settings";
    String FIND_FRIENDS = "Find Friends";
    String COMMENT_ERROR = "Comments field cannot be blank";
    String RESET_PASSWORD = "Reset Password";
    String RESET_EMAIL_ERROR = "Email field cannot be blank";
    String VISIT_USER_ID = "visit_user_id";
    String DEFAULT_FRIEND_STATUS = "not_friends";
    String REQUEST_TYPE = "request_type";
    String SENT = "send";
    String RECEIVED = "received";
    String REQUEST_SENT = "request_sent";
    String CANCEL_REQUEST = "CANCEL FRIEND REQUEST";
    String SEND_REQUEST = "SEND FRIEND REQUEST";
    String REQUEST_RECEIVED = "request_received";
    String ACCEPT_REQUEST = "ACCEPT FRIEND REQUEST";
    String FRIENDS = "friends";
    String UNFRIEND = "UNFRIEND";
    String USERNAME = "userName";
    String MESSAGE_ERROR = "Message field cannot be blank";
    String HOME = "Home";
}
