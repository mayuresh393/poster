package com.jagmit.android.postgram.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jagmit.android.postgram.Utils.Constants;
import com.jagmit.android.postgram.R;
import com.jagmit.android.postgram.Utils.FireBaseConstants;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;

public class PostActivity extends AppCompatActivity {
    private static final int RESULT_LOAD_IMAGE = 1;
    private ImageButton selectPostImageButton;
    private Button updatePostButton;
    private Uri imageUri;
    private EditText postDescriptionEditText;
    private StorageReference postImageReference;
    private String downloadUrl;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference userRef, postRef;
    private ProgressDialog progressDialog;
    private String postDescription, currentDate, currentTime, postRandomName, currentUserId;
    private long countPost = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        progressDialog = new ProgressDialog(PostActivity.this);
        firebaseAuth = FirebaseAuth.getInstance();
        currentUserId = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        userRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_USER);
        postRef = FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.NODE_POSTS);
        postImageReference = FirebaseStorage.getInstance().getReference().child("Post Images");


        initView();

        initListeners();


    }

    private void initListeners() {
        selectPostImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        updatePostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    progressDialog.setTitle("Please wait while updating new post.. ");
                    progressDialog.setCancelable(false);
                    progressDialog.setIndeterminate(true);
                    progressDialog.show();
                    saveImageToFirebaseStorage();
                }
            }
        });

    }

    private void saveImageToFirebaseStorage() {
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
        currentDate = simpleDateFormat.format(calendar.getTime());

        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm");
        currentTime = simpleTimeFormat.format(calendar.getTime());

        postRandomName = currentDate + currentTime;

        final StorageReference filepath = postImageReference.child(imageUri.getLastPathSegment() + postRandomName + ".jpg");

        filepath.putFile(imageUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (!task.isSuccessful()) {
                    progressDialog.dismiss();
                    Toast.makeText(PostActivity.this,"Error Occurred while uploading post "+ Objects.requireNonNull(task.getException()).getMessage(),Toast.LENGTH_SHORT).show();
                }
                return filepath.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri taskResult = task.getResult();
                    if (taskResult != null) {
                        downloadUrl = taskResult.toString();
                    }
                }
                else {
                    progressDialog.dismiss();
                    Toast.makeText(PostActivity.this,"Error Occurred while uploading post "+ Objects.requireNonNull(task.getException()).getMessage(),Toast.LENGTH_SHORT).show();
                }
                savingPostInformationToDatabase();
            }
        });
    }

    private void savingPostInformationToDatabase() {
        postRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    countPost = dataSnapshot.getChildrenCount();
                }
                else {
                    countPost = 0;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        userRef.child(currentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String fullname = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.FULL_NAME).getValue()).toString();
                    String userProfile = Objects.requireNonNull(dataSnapshot.child(FireBaseConstants.PROFILE_IMAGE).getValue()).toString();

                    HashMap<String, Object> postMap = new HashMap<>();
                    postMap.put(FireBaseConstants.UID, currentUserId);
                    postMap.put(FireBaseConstants.DATE, currentDate);
                    postMap.put(FireBaseConstants.TIME, currentTime);
                    postMap.put(FireBaseConstants.DESCRIPTION, postDescription);
                    postMap.put(FireBaseConstants.POST_IMAGE, downloadUrl);
                    postMap.put(FireBaseConstants.COUNTER,countPost);
                    postMap.put(FireBaseConstants.PROFILE_IMAGE, userProfile);
                    postMap.put(FireBaseConstants.FULL_NAME, fullname);

                    postRef.child(currentUserId + postRandomName).updateChildren(postMap)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    sendUserToHomeActivity();
                                    Toast.makeText(PostActivity.this, "New post is Updated Successfully", Toast.LENGTH_SHORT).show();
                                    progressDialog.dismiss();

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(PostActivity.this, "Error Occurred while updating post " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private boolean checkValidation() {
        if (imageUri == null) {
            Toast.makeText(PostActivity.this, "Please Select Post Image", Toast.LENGTH_SHORT).show();
            return false;
        }
        postDescription = postDescriptionEditText.getText().toString().trim();
        if (postDescription.isEmpty()) {
            postDescriptionEditText.setError(Constants.POST_ERROR);
            return false;
        }
        return true;
    }

    private void openGallery() {
        Intent imagePickerIntent = new Intent();
        imagePickerIntent.setAction(Intent.ACTION_GET_CONTENT);
        imagePickerIntent.setType("image/*");
        startActivityForResult(Intent.createChooser(imagePickerIntent, "Select Profile Pic"), RESULT_LOAD_IMAGE);
    }

    private void initView() {
        Toolbar toolbar = findViewById(R.id.postToolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.UPDATE_POST);

        selectPostImageButton = findViewById(R.id.imageBtnAdd);
        updatePostButton = findViewById(R.id.btnAddPost);
        postDescriptionEditText = findViewById(R.id.edtTextPostDescription);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
            imageUri = data.getData();
            selectPostImageButton.setImageURI(imageUri);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                sendUserToHomeActivity();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendUserToHomeActivity() {
        Intent homeIntent = new Intent(PostActivity.this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(homeIntent);
        finish();
    }
}
