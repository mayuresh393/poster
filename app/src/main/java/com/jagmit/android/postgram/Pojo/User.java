package com.jagmit.android.postgram.Pojo;

/**
 * Created by MAYURESH NIMBALKAR on 19/1/19.
 */
public class User {

    private String profileimage;
    private String fullname;
    private String status;

    public String getProfileimage() {
        return profileimage;
    }

    public String getFullname() {
        return fullname;
    }

    public String getStatus() {
        return status;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
