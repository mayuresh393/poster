package com.jagmit.android.postgram.CallBacks;
/**
 * Created by MAYURESH NIMBALKAR on 18/1/19.
 */
public interface OnRegisterCallBack {
    void onSuccess();
    void onFailure(String s);
}
